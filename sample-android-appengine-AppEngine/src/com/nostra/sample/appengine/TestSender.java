package com.nostra.sample.appengine;


import java.util.ArrayList;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

public class TestSender {
	  private static final String API_KEY = "AIzaSyCmM_gzJ5LWE-Hyue64St8rR_Fi-4DJNcQ";

	public static void main(String[] args) {
		try {
		      
	        Sender sender = new   Sender(API_KEY);//add your own google APIkey here
	        
	        // use this to send message with payload data
	        Message message = new Message.Builder()
	        .collapseKey("message")
	        .timeToLive(3) 
	        .delayWhileIdle(true)
	        .addData("message", "Welcome to Push Notifications") //you can get this message on client side app
	        .build();  
	        
	        //System.setProperty("https.proxyHost", "192.168.1.1");  //write your own proxyHost
	        //System.setProperty("https.proxyPort", "8080");     //write your own proxyHost
	       
	        //Use this code to send notification message to a single device
	        Result result = sender.send(message,
	           "APA91bFyQwhMmy4WCbq-ldCRY9ftjFs3CLs2CgHvxHlnmAaA-2JV1T73Wobvx93yliYwISSyYgqXKTnkHOlFNgEzANYgkU3ZH9BLiQY68SUN7sJQxkzNRx3eUeKKUD2oza455MpqGR6d9X8kZsbAUGBKr8LW_sMbJQ",
	           1);
	        
	        System.out.println("Message Result: "+result.toString()); //Print message result on console
	        
	        //Use this code to send notification message to multiple devices
	        ArrayList<String> devicesList = new ArrayList<String>();
	        
	        //add your devices RegisterationID, one for each device
	        devicesList.add("APA91bFyQwhMmy4WCbq-ldCRY9ftjFs3CLs2CgHvxHlnmAaA-2JV1T73Wobvx93yliYwISSyYgqXKTnkHOlFNgEzANYgkU3ZH9BLiQY68SUN7sJQxkzNRx3eUeKKUD2oza455MpqGR6d9X8kZsbAUGBKr8LW_sMbJQ");        
	        //devicesList.add("APA91bE2w5kK_LTmbm0vUL9VvaXfT5mqdIo9a719K_U18M1bbK2cTbbnQVhMsogxczRpoPEjeyExCkyPI19L1bJz2fBln-k_5yJA3T9-XRBceMyjai9cPYbEKVwBRbEuurpR0ki1LJfP");        
	        
	        //Use this code for multicast messages    
	        MulticastResult multicastResult = sender.send(message, devicesList, 0);
	        sender.send(message, devicesList, 0);        
	        System.out.println("Message Result: "+multicastResult.toString());//Print multicast message result on console
	        
	       } catch (Exception e) {
	        e.printStackTrace();
	       }
	}

}
